# Take A Shot

**Take A Shot** is a 2D-shooter game that can be played by at least two people. In this game, you have to take down all of the participants to become the winner. You can also talk to the other players via chat in the game!

> **About Game Take A Shot**

Genre: 2D, Shooter, Platformer

Mode: Multiplayer

Platform: Windows

This game was build using Unity 2D and Photon Unity Networking.

> **Sneak Peak To The Game**

- Join the game with your own username, then create or join the game room.
![](1-1.gif)
![](2-1.gif)

- Try to explore the game world and its gameplay.
![](2.gif)

- Chat with the other players.
![](3.gif)

- You can also leave the room as you please, and the other players will be notified as well.
![](4.gif)
